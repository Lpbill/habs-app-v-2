import requests
import json
import datetime
import time
import eel

# Constants for the HABS database
api_base_url = "https://api-web.nhle.com/v1/"
api_teams_url = api_base_url + "club-stats-season/"
api_schedule_url_part1 = api_base_url + "club-schedule/"
api_schedule_url_part2 = "/week/now"
api_game_url_part1 = api_base_url + "gamecenter/"
api_game_url_part2 = "/boxscore"
api_standings_url = api_base_url + "standings/"
api_team_stats_url = api_teams_url + "stats/"

# For daylight savings time
# 1 = daylight savings time
# 0 = standard time
daylightSavings = 1

# For changing team id for fun
teamABR = "MTL"

# list of teams and their ids and abbreviations
# to use this, use teams[teamABR]["id"] or teams[teamABR]["abbreviation"]
teams = {
    "New Jersey Devils": {"id": 1, "abbreviation": "NJD"},
    "New York Islanders": {"id": 2, "abbreviation": "NYI"},
    "New York Rangers": {"id": 3, "abbreviation": "NYR"},
    "Philadelphia Flyers": {"id": 4, "abbreviation": "PHI"},
    "Pittsburgh Penguins": {"id": 5, "abbreviation": "PIT"},
    "Boston Bruins": {"id": 6, "abbreviation": "BOS"},
    "Buffalo Sabres": {"id": 7, "abbreviation": "BUF"},
    "Montreal Canadiens": {"id": 8, "abbreviation": "MTL"},
    "Ottawa Senators": {"id": 9, "abbreviation": "OTT"},
    "Toronto Maple Leafs": {"id": 10, "abbreviation": "TOR"},
    "Carolina Hurricanes": {"id": 12, "abbreviation": "CAR"},
    "Florida Panthers": {"id": 13, "abbreviation": "FLA"},
    "Tampa Bay Lightning": {"id": 14, "abbreviation": "TBL"},
    "Washington Capitals": {"id": 15, "abbreviation": "WSH"},
    "Chicago Blackhawks": {"id": 16, "abbreviation": "CHI"},
    "Detroit Red Wings": {"id": 17, "abbreviation": "DET"},
    "Nashville Predators": {"id": 18, "abbreviation": "NSH"},
    "St. Louis Blues": {"id": 19, "abbreviation": "STL"},
    "Calgary Flames": {"id": 20, "abbreviation": "CGY"},
    "Colorado Avalanche": {"id": 21, "abbreviation": "COL"},
    "Edmonton Oilers": {"id": 22, "abbreviation": "EDM"},
    "Vancouver Canucks": {"id": 23, "abbreviation": "VAN"},
    "Anaheim Ducks": {"id": 24, "abbreviation": "ANA"},
    "Dallas Stars": {"id": 25, "abbreviation": "DAL"},
    "Los Angeles Kings": {"id": 26, "abbreviation": "LAK"},
    "San Jose Sharks": {"id": 28, "abbreviation": "SJS"},
    "Columbus Blue Jackets": {"id": 29, "abbreviation": "CBJ"},
    "Minnesota Wild": {"id": 30, "abbreviation": "MIN"},
    "Winnipeg Jets": {"id": 52, "abbreviation": "WPG"},
    "Arizona Coyotes": {"id": 53, "abbreviation": "ARI"},
    "Vegas Golden Knights": {"id": 54, "abbreviation": "VGK"},
    "Seattle Kraken": {"id": 55, "abbreviation": "SEA"},
}
# print name of team connected to an id
for team in teams:
    if teams[team]["id"] == 8:
        print(team)


lastGameDate = "2024-04-18"
firstGameDate = "2023-10-04"

for team in teams:
    if teams[team]["abbreviation"] == teamABR:
        teamName = team
        break


class HABS:
    def __init__(self):
        self.stats = {
            "teamName": teamName,
            "teamId": teams[teamName]["id"],
            "wins": 0,
            "losses": 0,
            "ot": 0,
            "points": 0,
            "conferenceRank": 0,
        }
        self.easternStandings = []
        self.nextGame = Game()
        self.lastGame = Game()
        self.currentGame = Game()
        self.advancedStats = {
            "goalsPerGame": 0,
            "goalsAgainstPerGame": 0,
            "powerPlayPercentage": 0,
            "penaltyKillPercentage": 0,
            "shotsPerGame": 0,
            "shotsAllowed": 0,
            "winScoreFirst": 0,
            "winOppScoreFirst": 0,
            "winLeadFirstPer": 0,
            "winLeadSecondPer": 0,
            "faceOffWinPercentage": 0,
        }

    def __str__(self):
        return (
            "Team: "
            + self.stats["teamName"]
            + "\n"
            + "Wins: "
            + str(self.stats["wins"])
            + "\n"
            + "Losses: "
            + str(self.stats["losses"])
            + "\n"
            + "Overtime Losses: "
            + str(self.stats["ot"])
            + "\n"
            + "Points: "
            + str(self.stats["points"])
            + "\n"
            + "Next Game: "
            + self.nextGame.gameDate
            + "\n"
            + "Last Game: "
            + self.lastGame.gameDate
            + "\n"
            + "Current Game: "
            + self.currentGame.gameDate
            + "\n"
            + "conferenceRank: "
            + str(self.stats["conferenceRank"])
            + "\n"
            + "Goals per game: "
            + str(self.advancedStats["goalsPerGame"])
            + "\n"
            + "Goals against per game: "
            + str(self.advancedStats["goalsAgainstPerGame"])
            + "\n"
            + "Power play percentage: "
            + str(self.advancedStats["powerPlayPercentage"])
            + "\n"
            + "Penalty kill percentage: "
            + str(self.advancedStats["penaltyKillPercentage"])
            + "\n"
            + "Shots per game: "
            + str(self.advancedStats["shotsPerGame"])
            + "\n"
            + "Shots allowed: "
            + str(self.advancedStats["shotsAllowed"])
            + "\n"
            + "Win score first: "
            + str(self.advancedStats["winScoreFirst"])
            + "\n"
            + "Win opp score first: "
            + str(self.advancedStats["winOppScoreFirst"])
            + "\n"
            + "Win lead first per: "
            + str(self.advancedStats["winLeadFirstPer"])
            + "\n"
            + "Win lead second per: "
            + str(self.advancedStats["winLeadSecondPer"])
            + "\n"
            + "Face off win percentage: "
            + str(self.advancedStats["faceOffWinPercentage"])
        )

    def getStats(self):
        # parse the stats from the api and store them. They come in a json format
        response_team_stats = requests.get(
            api_teams_url + str(self.stats["teamId"]) + "/stats"
        )
        response_team_stats_json = response_team_stats.json()
        self.stats["teamName"] = response_team_stats_json["stats"][0]["splits"][0][
            "team"
        ]["name"]
        self.stats["wins"] = response_team_stats_json["stats"][0]["splits"][0]["stat"][
            "wins"
        ]
        self.stats["losses"] = response_team_stats_json["stats"][0]["splits"][0][
            "stat"
        ]["losses"]
        self.stats["ot"] = response_team_stats_json["stats"][0]["splits"][0]["stat"][
            "ot"
        ]
        self.stats["points"] = response_team_stats_json["stats"][0]["splits"][0][
            "stat"
        ]["pts"]
        return self

    def getStandings(self):
        # parse the standings from the api and store them. They come in a json format
        response_team_standings = requests.get(api_standings_url + "byConference")
        response_team_standings_json = response_team_standings.json()
        list_of_eastern_teams = []
        for team in response_team_standings_json["records"][0]["teamRecords"]:
            list_of_eastern_teams.append(team["team"]["name"])
            if team["team"]["id"] == teamABR:
                self.stats["conferenceRank"] = team["conferenceRank"]
        self.easternStandings = list_of_eastern_teams
        return self

    def getNextGame(self):
        # parse the next game from the api and store it. They come in a json format
        self.nextGame = Game()
        self.nextGame.getNextGame()
        return self

    def getLastGame(self):
        # parse the last game from the api and store it. They come in a json format
        self.lastGame = Game()
        self.lastGame.getPreviousGame()
        return self

    def getCurrentGame(self):
        # parse the current game from the api and store it. They come in a json format
        currentGame = Game()
        currentGame.getLiveGame()
        self.currentGame = currentGame
        return self

    def getAdvancedStats(self):
        response_team_stats = requests.get(
            api_teams_url + str(self.stats["teamId"]) + "/stats"
        )
        response_team_stats_json = response_team_stats.json()
        self.advancedStats["goalsPerGame"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["goalsPerGame"]
        self.advancedStats["goalsAgainstPerGame"] = response_team_stats_json["stats"][
            0
        ]["splits"][0]["stat"]["goalsAgainstPerGame"]
        self.advancedStats["powerPlayPercentage"] = response_team_stats_json["stats"][
            0
        ]["splits"][0]["stat"]["powerPlayPercentage"]
        self.advancedStats["penaltyKillPercentage"] = response_team_stats_json["stats"][
            0
        ]["splits"][0]["stat"]["penaltyKillPercentage"]
        self.advancedStats["shotsPerGame"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["shotsPerGame"]
        self.advancedStats["shotsAllowed"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["shotsAllowed"]
        self.advancedStats["winScoreFirst"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["winScoreFirst"]
        self.advancedStats["winOppScoreFirst"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["winOppScoreFirst"]
        self.advancedStats["winLeadFirstPer"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["winLeadFirstPer"]
        self.advancedStats["winLeadSecondPer"] = response_team_stats_json["stats"][0][
            "splits"
        ][0]["stat"]["winLeadSecondPer"]
        self.advancedStats["faceOffWinPercentage"] = response_team_stats_json["stats"][
            0
        ]["splits"][0]["stat"]["faceOffWinPercentage"]
        return self


class Game:
    def __init__(self):
        self.gameId = 0
        self.status = "status"
        self.gameDate = "aaaa-mm-dd"
        self.Broadcasts = []
        self.homeTeamID = 0
        self.homeTeam = "Home Team"
        self.awayTeamID = 0
        self.awayTeam = "Away Team"
        self.StartTime = "00:00"
        self.Live = False
        self.LiveScore = "0-0"
        self.FinalScore = "0-0"

    def __str__(self):
        return (
            "Game ID: "
            + str(self.gameId)
            + "\nStatus: "
            + self.status
            + "\nGame Date: "
            + self.gameDate
            + "\nBroadcasts: "
            + str(self.Broadcasts)
            + "\nHome Team ID: "
            + str(self.homeTeamID)
            + "\nHome Team: "
            + self.homeTeam
            + "\nAway Team ID: "
            + str(self.awayTeamID)
            + "\nAway Team: "
            + self.awayTeam
            + "\nStart Time: "
            + self.StartTime
            + "\nLive: "
            + str(self.Live)
            + "\nLive Score: "
            + self.LiveScore
            + "\nFinal Score: "
            + self.FinalScore
        )

    def getNextGame(self):
        response_schedule = requests.get(
            api_schedule_url_part1 + teamABR + api_schedule_url_part2
        )
        response_schedule_json = response_schedule.json()
        nextStartDate = response_schedule_json["nextStartDate"]  # in format yyyy-mm-dd
        previousStartDate = response_schedule_json[
            "previousStartDate"
        ]  # in format yyyy-mm-dd
        gameFound = False
        while not gameFound:
            for game in response_schedule_json["games"]:
                if game["gameState"] == "FUT":
                    self.status = game["gameState"]
                    self.gameDate = game["gameDate"]
                    self.gameId = game["id"]
                    self.Broadcasts = []
                    for broadcast in game["tvBroadcasts"]:
                        self.Broadcasts.append(broadcast["network"])
                    self.homeTeamID = game["homeTeam"]["id"]
                    for team in teams:
                        if teams[team]["id"] == self.homeTeamID:
                            self.homeTeam = team
                            break
                    self.awayTeamID = game["awayTeam"]["id"]
                    for team in teams:
                        if teams[team]["id"] == self.awayTeamID:
                            self.awayTeam = team
                            break
                    self.StartTime = game["startTimeUTC"]
                    self.Live = False
                    self.LiveScore = "n/a"
                    self.FinalScore = "n/a"
                    gameFound = True
                    return self
            if not gameFound:
                response_schedule = requests.get(
                    api_schedule_url_part1 + teamABR + "/" + nextStartDate
                )
                response_schedule_json = response_schedule.json()
                nextStartDate = response_schedule_json[nextStartDate]
                previousStartDate = response_schedule_json[previousStartDate]

    def getPreviousGame(self):
        response_schedule = requests.get(
            api_schedule_url_part1 + teamABR + api_schedule_url_part2
        )
        response_schedule_json = response_schedule.json()
        nextStartDate = response_schedule_json["nextStartDate"]
        previousStartDate = response_schedule_json["previousStartDate"]
        gameFound = False
        while not gameFound:
            # starts backwards in the list of games
            for game in reversed(response_schedule_json["games"]):
                if game["gameState"] == "OFF":
                    self.status = game["gameState"]
                    self.gameDate = game["gameDate"]
                    self.gameId = game["id"]
                    self.Broadcasts = []
                    for broadcast in game["tvBroadcasts"]:
                        self.Broadcasts.append(broadcast["network"])
                    self.homeTeamID = game["homeTeam"]["id"]
                    for team in teams:
                        if teams[team]["id"] == self.homeTeamID:
                            self.homeTeam = team
                            break
                    self.awayTeamID = game["awayTeam"]["id"]
                    for team in teams:
                        if teams[team]["id"] == self.awayTeamID:
                            self.awayTeam = team
                            break
                    self.StartTime = game["startTimeUTC"]
                    self.Live = False
                    self.LiveScore = "n/a"
                    response_game = requests.get(
                        api_game_url_part1 + str(self.gameId) + api_game_url_part2
                    )
                    response_game_json = response_game.json()
                    self.FinalScore = (
                        str(response_game_json["homeTeam"]["score"])
                        + "-"
                        + str(response_game_json["awayTeam"]["score"])
                    )
                    gameFound = True
                    return self
            if not gameFound:
                response_schedule = requests.get(
                    api_schedule_url_part1 + teamABR + "/week/" + previousStartDate
                )
                response_schedule_json = response_schedule.json()
                nextStartDate = response_schedule_json["nextStartDate"]
                previousStartDate = response_schedule_json["previousStartDate"]

    def getLiveGame(self):
        response_schedule = requests.get(
            api_schedule_url_part1 + teamABR + api_schedule_url_part2
        )
        response_schedule_json = response_schedule.json()
        gameFound = False
        for game in response_schedule_json["games"]:
            if game["gameState"] == "LIVE" :
                self.status = game["gameState"]
                self.gameDate = game["gameDate"]
                self.gameId = game["id"]
                self.Broadcasts = []
                for broadcast in game["tvBroadcasts"]:
                    self.Broadcasts.append(broadcast["network"])
                self.homeTeamID = game["homeTeam"]["id"]
                for team in teams:
                    if teams[team]["id"] == self.homeTeamID:
                        self.homeTeam = team
                        break
                self.awayTeamID = game["awayTeam"]["id"]
                for team in teams:
                    if teams[team]["id"] == self.awayTeamID:
                        self.awayTeam = team
                        break
                self.StartTime = game["startTimeUTC"]
                self.Live = True
                self.LiveScore = (
                    str(game["homeTeam"]["score"]) + "-" + str(game["awayTeam"]["score"])
                )
                self.FinalScore = "n/a"
                gameFound = True
                return self
            if game["gameState"] == "PRE" :
                self.status = game["gameState"]
                self.live = True
                self.gameDate = game["gameDate"]
                self.gameId = game["id"]
                self.Broadcasts = []
                for broadcast in game["tvBroadcasts"]:
                    self.Broadcasts.append(broadcast["network"])
                self.homeTeamID = game["homeTeam"]["id"]
                for team in teams:
                    if teams[team]["id"] == self.homeTeamID:
                        self.homeTeam = team
                        break
                self.awayTeamID = game["awayTeam"]["id"]
                for team in teams:
                    if teams[team]["id"] == self.awayTeamID:
                        self.awayTeam = team
                        break
                self.StartTime = game["startTimeUTC"]
                self.LiveScore = "n/a"
                self.FinalScore = "n/a"
        return None


eel.init("interface")
# create a habs object
habs = HABS()


@eel.expose
def get_all_stats():
    print("get_all_stats")
    # get the next game date
    # habs.getStats()
    # habs.getAdvancedStats()
    habs.getNextGame()
    habs.getCurrentGame()
    habs.getLastGame()
    # habs.getStandings()
    # print the habs class in the console
    print(habs.nextGame)
    # print("-------------------------------")
    # print("Standings")
    # print("-------------------------------")
    # place = 1
    # for team in habs.easternStandings:
    #     print(str(place) + " - " + team)
    #     place += 1
    # print("-------------------------------")
    # print("Logs")
    # print("-------------------------------")


@eel.expose
def get_next_game_date():
    print("get_next_game_date")
    return habs.nextGame.gameDate


@eel.expose
def get_next_teams():
    print("get_next_teams")
    teams = []
    teams.append(habs.nextGame.homeTeam)
    teams.append(habs.nextGame.awayTeam)
    return teams


@eel.expose
def get_next_game_time():
    print("get_next_game_time")
    # from aaaa-mm-ddThh:mm:ssZ to hh:mm
    time = habs.nextGame.StartTime[11:16]
    # change the time zone from UTC to EST
    time = datetime.datetime.strptime(time, "%H:%M")
    time = time - datetime.timedelta(hours=4)
    time = datetime.datetime.strftime(time, "%H:%M")

    # take into account daylight saving time
    if daylightSavings:
        time = datetime.datetime.strptime(time, "%H:%M")
        time = time - datetime.timedelta(hours=1)
        time = datetime.datetime.strftime(time, "%H:%M")

    return time


@eel.expose
def get_next_broadcasts():
    print("get_next_broadcasts")
    # create a broadcasts string
    broadcasts = ""
    for broadcast in habs.nextGame.Broadcasts:
        broadcasts += broadcast + ", "

    # remove the last ", "
    broadcasts = broadcasts[:-2]
    return broadcasts


@eel.expose
def get_next_team_id():
    print("get_next_team_id")
    if habs.nextGame.homeTeamID == teams[teamName]["id"]:
        return habs.nextGame.awayTeamID
    else:
        return habs.nextGame.homeTeamID


@eel.expose
def get_last_game_date():
    print("get_last_game_date")
    return habs.lastGame.gameDate


@eel.expose
def get_last_team_id():
    print("get_last_team_id")
    if habs.lastGame.homeTeamID == teams[teamName]["id"]:
        return habs.lastGame.awayTeamID
    else:
        return habs.lastGame.homeTeamID


@eel.expose
def get_last_teams():
    print("get_last_teams")
    teams = []
    teams.append(habs.lastGame.homeTeam)
    teams.append(habs.lastGame.awayTeam)
    return teams


@eel.expose
def get_last_final_score():
    print("get_last_final_score")
    return habs.lastGame.FinalScore


@eel.expose
def get_live_status():
    print("get_live_status")
    return habs.currentGame.Live


@eel.expose
def get_live_start_time():
    print("get_live_start_time")
    # from aaaa-mm-ddThh:mm:ssZ to hh:mm
    time = habs.currentGame.StartTime[11:16]
    # change the time zone from UTC to EST
    time = datetime.datetime.strptime(time, "%H:%M")
    time = time - datetime.timedelta(hours=4)
    time = datetime.datetime.strftime(time, "%H:%M")
    # take into account daylight saving time
    if daylightSavings:
        time = datetime.datetime.strptime(time, "%H:%M")
        time = time - datetime.timedelta(hours=1)
        time = datetime.datetime.strftime(time, "%H:%M")
    return time


@eel.expose
def get_live_teams():
    print("get_live_teams")
    teams = []
    teams.append(habs.currentGame.homeTeam)
    teams.append(habs.currentGame.awayTeam)
    return teams


@eel.expose
def get_live_team_id():
    print("get_live_team_id")
    if habs.currentGame.homeTeamID == teams[teamName]["id"]:
        return habs.currentGame.awayTeamID
    else:
        return habs.currentGame.homeTeamID


@eel.expose
def get_live_score():
    print("get_live_score")
    return habs.currentGame.LiveScore


@eel.expose
def get_live_broadcasts():
    print("get_live_broadcasts")
    # create a broadcasts string
    broadcasts = ""
    for broadcast in habs.currentGame.Broadcasts:
        broadcasts += broadcast + ", "

    # remove the last ", "
    broadcasts = broadcasts[:-2]
    return broadcasts


@eel.expose
def get_wins_losses_ot_points():
    print("get_WLOTP_stats")
    stats = []
    stats.append(habs.stats["wins"])
    stats.append(habs.stats["losses"])
    stats.append(habs.stats["ot"])
    stats.append(habs.stats["points"])
    stats.append(habs.stats["conferenceRank"])
    return stats


eel.start("index.html", size=(1000, 600))
